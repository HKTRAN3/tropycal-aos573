{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "00eaf5f6-31e1-4ce4-ba3e-c3b992506b69",
   "metadata": {},
   "source": [
    "<b><font size=20, color='steelblue'>Tropycal</font></b>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b43c18d-0e50-4117-a997-83d4bd7b2c38",
   "metadata": {},
   "source": [
    "Quinn Bowman and Harrison Tran<br>\n",
    "12 / 7 / 2022<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "106478e4-a25f-4199-be04-b6f45561e285",
   "metadata": {},
   "source": [
    "#### <span style=\"color:green\">Learning Goals</span>\n",
    "<div style=\"background-color:#eaffe8; border:2px solid green; padding: 1em\">\n",
    "By the end of this notebook you will\n",
    "<ol>\n",
    "    <li>Understand how to fetch and use tropical cyclone data using <b>Tropycal</b></li>\n",
    "    <li>Make simple plots of tropical cyclone data using built-in methods and integrate tropcial cyclone data into your workflows.</li>\n",
    "</ol>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de75bb85-97d4-4ea2-b33f-a848c4317335",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### Resources\n",
    "##### **Further information about Tropycal**\n",
    "- [ ] [Tropycal documentation](https://tropycal.github.io/tropycal/)\n",
    "- [ ] [Additional tropycal tutorials](https://tropycal.github.io/tropycal/examples/index.html) -- this was a starting point for some of the workflows presented here!\n",
    "- [ ] [Tropycal Python package index (PyPI) page](https://pypi.org/project/tropycal/)\n",
    "- [ ] [Tropycal presentation at the 34th Conference on Hurricanes and Tropical Meteorology](https://ams.confex.com/ams/34HURR/meetingapp.cgi/Paper/373965)\n",
    "- [ ] [Tropycal on GitHub](https://github.com/tropycal/tropycal) (it's open source!)\n",
    "\n",
    "##### **Further reading on the data being used**\n",
    "- [ ] [The International Best Track Archive for Climate Stewardship](https://www.ncei.noaa.gov/products/international-best-track-archive) (IBTrACS)\n",
    "- [ ] [HURDAT](https://en.wikipedia.org/wiki/HURDAT) (Wikipedia article)\n",
    "- [ ] [High Density Observations (HDOB)](https://www.nhc.noaa.gov/abouthdobs_2007.shtml)\n",
    "- [ ] [How the National Hurricane Center uses recon data](https://severeweather.wmo.int/TCFW/RAIV_Workshop2022/12_RAIV-HurricaneWorkshop2022_UseAircraftData_JamesFranklin.pdf) (PDF)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48291450-eb97-4f26-94f9-32c826bd8a16",
   "metadata": {},
   "source": [
    "# A little about Tropycal"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13ff3384-4328-4db0-aa72-01ad77f8778f",
   "metadata": {},
   "source": [
    "**[Tropycal](https://tropycal.github.io/tropycal/)** is a newly-developed Python package designed to make it easier for a user to fetch and analyze tropical cyclone data. Under the auspices of the World Meteorological Organization, various Tropical Cyclone Warning Centers (TCWCs) are tasked with monitoring and warning on tropical cyclones (known locally as cyclones, hurricanes, or typhoons depending on where they form). Each TCWC maintains its own database on historical storms, which can make it cumbersome for a user to consolidate and analyze. That's where `Tropycal` steps in: to help make this data gathering process a little easier. Additionally, Tropycal has other cool features, such as real-time functionality, aircraft reconaissance processing, tropical cyclone model tracks, and other tools to help a user view tropical cyclone data.\n",
    "\n",
    "Even if your research might not specifically involve tropical cyclones, hopefully if you're ever in a pinch and need a good way of quickly fetching tropical cyclone data, you can turn to `Tropycal`. Relevant applications beyond the realm of tropical cyclones that could find `Tropycal` useful can be found broadly in other parts of tropical meteorology, such as in studying tropical convection and the impact of climate teleconnections, as well as in research on the impacts of weather on society and ecology."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd6f8ca2-dedd-4e18-a6a9-cba0aa6decc9",
   "metadata": {},
   "source": [
    "We'll be touching upon various facets of `Tropycal` in this mini-tutorial. The `Tropycal` package offers several helpful functions that are divvied up into six *modules*, and we'll be touching upon some of them today. To start, let's import those modules, in addition to some other packages that will be useful:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "329f43b8-ec4a-4e6e-9c43-7d65be367be8",
   "metadata": {},
   "outputs": [],
   "source": [
    "import tropycal.tracks as tracks      # Read/analyze tropical cyclone track and intensity data\n",
    "import tropycal.tornado as tornado    # Read/analyze tornado data\n",
    "import tropycal.rain as rain          # Read/analyze tropical cyclone rainfall data\n",
    "import tropycal.recon as recon        # Read/analyze tropical cyclone aircraft reconnaissance data\n",
    "import tropycal.realtime as realtime  # Tools for active tropical cyclones\n",
    "import tropycal.utils                 # Additional analysis tools for tropical cyclones\n",
    "\n",
    "import pandas as pd\n",
    "import datetime as dt\n",
    "import matplotlib.pyplot as plt\n",
    "import cartopy.crs as ccrs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8d3daf4-597c-4223-9315-d20beee401be",
   "metadata": {},
   "source": [
    "Loading tropical cyclone data takes time, so while we discuss this first part of the demo, you can run the following code segment which will be useful later"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c735245d-3c06-4a9a-afdd-2bd05cec1152",
   "metadata": {},
   "outputs": [],
   "source": [
    "atl_basin = tracks.TrackDataset(basin='north_atlantic',include_btk=False, interpolate_data=True); # Load data for the North Atlantic"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e23e5454-3d4f-42af-a81e-8dce1c075c48",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa1ed166-cc11-413a-9f8b-e4a1c355e891",
   "metadata": {},
   "source": [
    "### A brief note on how tropical cyclone data is organized..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "106cc43d-251c-4f21-9558-03b081ce2a2f",
   "metadata": {
    "tags": []
   },
   "source": [
    "Tropical cyclones are clustered around particular areas of the world's oceans. These are known as **basins**, and each basin has a meteorological agency tasked with monitoring the storms in that basin, which is why tropical cyclone data tends to be sorted into basins. Storms within a basin are grouped within **seasons**, which define the year in which they formed. Tropycal shares its vocabulary with what's commonly used in tropical cyclone forecasting applications, so a review of these terms may be helpful."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bdbd5631-e3f4-4fed-b595-04f1606ebdf6",
   "metadata": {},
   "source": [
    "| Basin      | Tropycal string | Further notes\n",
    "| -- | -- | -- |\n",
    "| North Atlantic | \"north_atlantic\" | |\n",
    "| Northeast Pacific | \"east_pacific\" | |\n",
    "| North Atlantic and Northeast Pacific | \"both\" | These are the basins under the responsibility of the National Hurricane Center in Miami, FL\n",
    "| West Pacific | \"west_pacific\" | |\n",
    "| Australia | \"australia\" | |\n",
    "| North Indian | \"north_indian\" | |\n",
    "| South Indian | \"south_indian\" | |\n",
    "| South Atlantic | \"south_atlantic\" | |\n",
    "| all basins | \"all\" | |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0ef2fca1-348d-41a9-9885-a62cee21d1bd",
   "metadata": {},
   "source": [
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1319df5f-f4ca-4ed7-ad11-60d9e937cb8c",
   "metadata": {},
   "source": [
    "## 1. Using Tropycal for Reading and Displaying Storm Tracks"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b88a61ae-0766-46fb-b2d5-54fbf0c2339c",
   "metadata": {},
   "source": [
    "Tropycal can analyze entire tropical storm seasons, in any major basin.  Here we will load in the East Pacific."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc3ab3ef-d381-4110-a421-de89a452423c",
   "metadata": {},
   "outputs": [],
   "source": [
    "basin = tracks.TrackDataset(basin='east_pacific',source='hurdat',include_btk=True) # loads in entire East Pacific storm history.  Include btk includes the yet-unfinish 2022 season."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ef76a7d9-0113-4316-b73b-f95e38251cde",
   "metadata": {},
   "outputs": [],
   "source": [
    "season = basin.get_season(2002) #loads the 2002 season"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3dc474aa-e40a-40f2-bd45-2c8768daaa2c",
   "metadata": {},
   "outputs": [],
   "source": [
    "season"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed31089f-b789-4d6a-ae2b-7019fc337c8c",
   "metadata": {},
   "source": [
    "A season object can easily be converted into a pandas dataframe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5cd1eae4-9ffb-455a-a764-2a8073273817",
   "metadata": {},
   "outputs": [],
   "source": [
    "df = season.to_dataframe()\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4974236a-6dac-4cdb-8135-dc109df215cd",
   "metadata": {},
   "source": [
    "However, tropycal has many built in functions with its Season objects, so for now we will just use that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a21515ee-1124-4d3e-86c6-54fed762693a",
   "metadata": {},
   "outputs": [],
   "source": [
    "season.plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91483b16-9513-4987-9b89-5cd5be253cde",
   "metadata": {},
   "outputs": [],
   "source": [
    "Elida = season.get_storm(('Elida',2002))\n",
    "Elida"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a38ab0f-7401-4a8d-927f-7532b03dfaf5",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = Elida.to_xarray()\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81f75b48-fb95-4980-ae7e-b0848c17b625",
   "metadata": {},
   "source": [
    "Let's plot just storm Elida"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51e5523c-33a0-42d7-a677-6e8ce96241da",
   "metadata": {},
   "outputs": [],
   "source": [
    "Elida.plot(prop={'dots':True,'fillcolor':'vmax','ms':5.5})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c410261-36ac-473d-ad0c-e164c1f0111b",
   "metadata": {},
   "source": [
    "Here, the plot is color coded for max windspeed, but there are many other options as well"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ff2aff51-5897-4ef8-bd02-585e50d6c3de",
   "metadata": {},
   "outputs": [],
   "source": [
    "Elida.plot(prop={'dots':False,'linecolor':'mslp','ms':5.5,'linewidth':4.0}) "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17986211-7e5a-4f22-9e9e-cb6d08064e3f",
   "metadata": {},
   "source": [
    "You can even combine different seasons for simultaneous plotting and analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "61fd438a-5dec-47ac-bdb3-ce3ea7ed7682",
   "metadata": {},
   "outputs": [],
   "source": [
    "season1 = basin.get_season(2002)\n",
    "season2 = basin.get_season(2003)\n",
    "\n",
    "combined = season1 + season2\n",
    "combined.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13bbd8d7-373a-479f-9dbf-d5b0f410b190",
   "metadata": {},
   "source": [
    "### Realtime data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd597766-9aa3-43cd-9122-57c6a8ea294a",
   "metadata": {},
   "source": [
    "While the HURDAT2 database used for the previous work is for past storms, tropycal can also be used to work with current (realtime)storms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bddf35cf-9cc2-482e-9365-f6808276fe4c",
   "metadata": {},
   "outputs": [],
   "source": [
    "realtime_obj = realtime.Realtime()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3596fd5c-3ed2-4d33-9e1f-d95bc405044b",
   "metadata": {},
   "outputs": [],
   "source": [
    "realtime_obj"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd5b0da2-3f79-4416-8f70-c6e146227334",
   "metadata": {},
   "source": [
    "Storm and Season Data analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6771302-4682-4c97-83f4-561f75530836",
   "metadata": {},
   "source": [
    "However, there are no currently active storms ( As of Dec 2) :/ \n",
    "(see https://tropycal.github.io/tropycal/examples/realtime.html for uses during the hurricane season)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bbd276a7-088f-4e68-999b-b08bf9660d95",
   "metadata": {},
   "source": [
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3733cafb-59b7-447d-8d43-7ebe5c09577e",
   "metadata": {},
   "source": [
    "## 2. Climatalogical statistics"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34c1de2e-f76b-49ac-bd21-5fc5fb4346bc",
   "metadata": {},
   "source": [
    "Tropycal can also be used to look into storm season statistics and climatology over time.  The plot below shows the 2022 hurricane season Accumulated Cyclone Energy (ACE) compared to 2002, as well as average climatology and statistical measures of ACE from the beginning of the dataset.  As you will note, the 2022 season is still technically ongoing, and it was a higher than average year energetically."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca414224-9c30-417a-b057-9c7b54663952",
   "metadata": {},
   "outputs": [],
   "source": [
    "basin.ace_climo(plot_year=2022,compare_years=2002)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad4d0f1b-953b-4e57-b6cd-d3a83f1ad3e0",
   "metadata": {},
   "source": [
    "Rather than a cumulative total, Tropycal can also plot a running sum over time in order to show the change over the course of the season"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "79bfbfd4-55ff-4451-bb04-989b7c3da35d",
   "metadata": {},
   "outputs": [],
   "source": [
    "basin.ace_climo(rolling_sum=30,plot_year=2022,compare_years=2002)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67776b74-e1cc-44b8-97df-6602a523f818",
   "metadata": {},
   "source": [
    "As you can see, the 2022 season was very active early, but relatively inactive in august and the beginning of September, which is usually the peak of hurricane activity!  This plot also shows the peaks in ACE at a given time.  Both had very similar peaks in ACE, but the times were quite different."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2969ce1-66b2-444c-9da3-dc12b87a2bfd",
   "metadata": {},
   "source": [
    "Rather than Cyclone energy, the number of days with active cyclones can be plotted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f82a0a61-db50-4920-8c86-d3b1a477703e",
   "metadata": {},
   "outputs": [],
   "source": [
    "basin.hurricane_days_climo(plot_year=2022)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a22ebbf0-ff88-49ed-83c7-881cc4ebe829",
   "metadata": {},
   "source": [
    "Combined with previous plots, one can see that 2022 had far more days and and far more intense storms in the beginning of the season, while the end of the season in particular had more tropical storms rather than higher intensity events."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af135452-1c16-4854-ba18-33091e270a72",
   "metadata": {},
   "source": [
    "Another hurricane data variable of note is the relationship between a tropical Cyclone's maximum wind and minimum sea level pressure.  These are typically very well correlated, but occasionally are not.  This plot shows the average relationship and a frequency heatmap, compared to an input storm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72f534d3-e8f1-4777-ae4f-bb57678443f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "basin.wind_pres_relationship(storm=('Darby',2022))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d7d33f7-6c45-4c65-aa42-ff8fccf79512",
   "metadata": {},
   "source": [
    "Compared to average, Darby has higher pressures for its wind speeds.  I don't have any background with tropical cyclones so I don't know what that means!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee1165c3-31a7-4287-83d0-b84904e2a846",
   "metadata": {},
   "source": [
    "Finally, tropycal can also do work with and create large gridded datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "131740e9-9066-44d4-8f28-5c2687ac363a",
   "metadata": {},
   "outputs": [],
   "source": [
    "da = basin.gridded_stats(request=\"90th percentile wind\",prop={'cmap':'bwr','clevs':[30,130],},return_array=True) #return_array causes the output to be returned as a datarray"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7634c32e-6a79-4de4-8588-3f39a73cf094",
   "metadata": {},
   "outputs": [],
   "source": [
    "da"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2d50e029-2c1e-46c2-8aa0-133e40abedd9",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "fig = plt.figure(figsize=(5,5))\n",
    "ax = fig.add_subplot(111)\n",
    "da.plot(ax=ax)\n",
    "ax.set_xlim(160,280)\n",
    "ax.set_ylim(5,30)\n",
    "ax.set_title('Data Array output of the tropycal gridded_stats function')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5767dfda-25f7-4d1c-877b-9452c3d73d35",
   "metadata": {},
   "source": [
    "As you can see, most tropycal functions return a figure with built in style, but it can also output data for external plotting.  The above data is a gridded datset that displays the 90th percentile winds in each 1 x 1 degree space.\n",
    "\n",
    "This function is extremely powerful and allows you to organize and sort datasets with just a string description!  See https://tropycal.github.io/tropycal/api/generated/tropycal.tracks.TrackDataset.gridded_stats.html#tropycal.tracks.TrackDataset.gridded_stats for more information."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e776694-09b1-41fe-9ffc-9587ce0ea291",
   "metadata": {},
   "source": [
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bab985d9-5b6c-4422-bfaf-a1268128f196",
   "metadata": {},
   "source": [
    "## 3. Loading and viewing reconnaissance data\n",
    "As risky as it seems, [flights are regularly flown into tropical cyclones](https://www.omao.noaa.gov/learn/aircraft-operations/about/hurricane-hunters) to gather information on their strength and behavior, both for informing forecasters and to collect data for scientific research. While at various points historically this has been done worldwide, the National Oceanic and Atmospheric Administration (NOAA), with the aid of the U.S. Air Force, has carried out the most flights into tropical cyclones in recent years. Flights performed to investigate storms for forecasting purposes are known as \"aircraft reconnaissance\", or *recon*."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a27e39cd-ee96-4f56-b378-9b7aa23829c8",
   "metadata": {},
   "source": [
    "Tropycal's recon tools are under the `tropycal.recon` module. Tropycal can load reconnaissance data based on the storm of interest, so let's load up a storm. We'll look at [Hurricane Patricia from 2015](https://en.wikipedia.org/wiki/Hurricane_Patricia), the strongest hurricane in the eastern Pacific on record.\n",
    "\n",
    "><b>Note:</b> A user can also access reconnaissance data using the `recon.ReconDataset(storm)` command, where `storm` is a `tropycal.tracks.storm.Storm` object. However, the method we're using today is the easiest and recommended method.</b>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e28743c1-3a66-40ca-9dc2-0c48bed75848",
   "metadata": {},
   "outputs": [],
   "source": [
    "# epac = tracks.TrackDataset( 'east_pacific' ) # Load the track dataset for the Eastern Pacific\n",
    "storm = basin.get_storm( ( 'Patricia', 2015 ) ) # Load Hurricane Patricia from the dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4f1804d-cc74-4e8d-9bd6-c3b014fefa73",
   "metadata": {},
   "source": [
    "It's now time to load in the recon data. To do this, we will use the built in data collection functions to get various types of recon data. These don't have to be saved to a variable because the commands modify the `storm.recon` object, which is initially blank. This process takes about 10 seconds or so. The **Resources** section of this notebook provides some helpful links that may be useful for understanding these reconnaissance data types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "076fa4e2-f1c0-484b-9edf-4dd1841508ba",
   "metadata": {},
   "outputs": [],
   "source": [
    "storm.recon.get_hdobs()           # Get High-Density Observations (HDOBs): meteorological and positional data collected by planes every 30 seconds\n",
    "storm.recon.get_dropsondes()      # Get dropsondes: data from insturment packages that are jettisoned from planes that measure vertical profiles\n",
    "storm.recon.get_vdms()            # Get Vortex Data Messages (VMDs): summaries of meteorological data transmitted each time a plane completes a flight through a storm's center"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08311b00-0574-478c-93e3-13628497dae5",
   "metadata": {},
   "source": [
    "If we print `storm.recon`, we'll now see that the data has been populated by recon data from Hurricane Patricia, along with some helpful summary information for each of the data types:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c4abb8d2-1763-474f-bff7-8818d47dc528",
   "metadata": {},
   "outputs": [],
   "source": [
    "storm.recon"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "edb0c467-684d-426a-ab00-02ba166065e3",
   "metadata": {},
   "source": [
    "Like many of its other data types, `Tropycal` also offers quick, built-in ways to visualize the recon data. `plot_summary()` shows a map of reconnaissance flight paths (colored by the winds they measured), the locations of dropsonde deployments (marked as triangles), and locations where flights located the center of the storm (marked as hexagons).\n",
    "\n",
    "><b><font color='red'>Note:</font> `plot_summary()` generates a map without a legend indicating what the colors are (flight-level wind speeds, in knots) or what any of the symbols represent!</b>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d3acdf6-d0d5-4e4c-9223-e02790b26edb",
   "metadata": {},
   "outputs": [],
   "source": [
    "storm.recon.plot_summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "629fd908-146a-47f4-9256-43ba60055d09",
   "metadata": {},
   "source": [
    "There are some other built-in plotting methods for the individual types of reconnaissance data. The Tropycal documentation provides a list of built-in plotting methods for [HDOBs](https://tropycal.github.io/tropycal/api/generated/tropycal.recon.hdobs.html#tropycal.recon.hdobs), [VDMs](https://tropycal.github.io/tropycal/api/generated/tropycal.recon.vdms.html#tropycal.recon.vdms), and [Dropsondes](https://tropycal.github.io/tropycal/api/generated/tropycal.recon.dropsondes.html#tropycal.recon.dropsondes). For instance, we can use the built-in method `hdobs.plot_hovmoller()` to plot an interpolated Hovmoller diagram of surface-level wind speeds measured by recon as a function of time and distance from the center of the storm:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d0b36dcf-9fc5-4f38-9109-50841a86d9dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "storm.recon.hdobs.plot_hovmoller( varname = 'sfmr', radlim = 100 )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25cc9d49-e3d9-401a-8910-7fe633ae818e",
   "metadata": {},
   "source": [
    "Since all reconnaissance data is associated with a geographic location, HDOBs, VDMs, and dropsondes all support the `.plot_points()` function, which can be [configured](https://tropycal.github.io/tropycal/api/generated/tropycal.recon.hdobs.plot_points.html#tropycal.recon.hdobs.plot_points) to show different variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8e2af486-6277-4eb5-a48d-ea16649121f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "storm.recon.hdobs.plot_points('p_sfc')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88cd98e1-d247-46a9-ac76-26bfd3d50877",
   "metadata": {},
   "source": [
    "These built-in methods are a great way to quickly view recon data, but within your research and data processing workflows, you would probably want to look at the underlying data specifically. Let's find a particular recon mission into Patricia to examine. First, we can get a list of mission IDs using the `recon.find_mission()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b4cd0ac-fb19-4b88-8ff5-14b13758f45e",
   "metadata": {},
   "outputs": [],
   "source": [
    "storm.recon.find_mission()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2708198d-a68c-4a2e-b3e8-5b1361adcc76",
   "metadata": {},
   "source": [
    "Let's select the last mission and print information on it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9a48d0e0-7a7f-470d-8192-285d917e04be",
   "metadata": {},
   "outputs": [],
   "source": [
    "mission_data = storm.recon.get_mission('03')\n",
    "print( mission_data )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09faad0f-2035-4705-9506-0510219e16b2",
   "metadata": {},
   "source": [
    "To get a sense of the data involved with this mission, we can turn to the `recon.plot_summary()` function again, but this time specify the mission:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e9a0ab67-0139-4627-8123-a999df72d9dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "storm.recon.plot_summary(mission='03')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "713cebe0-e6d1-46ad-9bd0-61cdf8c1b63a",
   "metadata": {},
   "outputs": [],
   "source": [
    "dropsondes = mission_data.dropsondes # Get list of dropsondes\n",
    "# print( dropsondes ) # You can check out the data here\n",
    "dropsonde3 = dropsondes[3]\n",
    "dropsonde3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4190ff7-baf0-4092-9c3a-3633c5e77d48",
   "metadata": {},
   "source": [
    "The meteorological data associated with the dropsonde is stored under the `levels` key, which is stored as a Pandas dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bdb071ae-17da-4ed8-8a29-5971a0da596e",
   "metadata": {},
   "outputs": [],
   "source": [
    "dropsonde3_data = dropsonde3['levels']\n",
    "dropsonde3_data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f833d797-7bc0-43da-8d3a-3d82e58d2fb6",
   "metadata": {},
   "source": [
    "Tropycal offers several ways of locating a specific set of recon data. For example, in the below cell we get the entire list of dropsondes associated with Hurricane Patricia, but then use the [`.sel()`](https://tropycal.github.io/tropycal/api/generated/tropycal.recon.dropsondes.sel.html#tropycal.recon.dropsondes.sel) function to locate only the dropsondes that were deployed in the center of the storm on October 23, 2015:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d27619c-2ce3-4e88-b029-d0e6ab711dc3",
   "metadata": {},
   "outputs": [],
   "source": [
    "dropsondes = storm.recon.dropsondes # Get list of dropsondes from the storm's entire lifetime\n",
    "center_dropsondes = dropsondes.sel(time=dt.datetime(2015,10,23),location='center') # Select only the dropsondes that were deployed in the center of the hurricane on 10/23/2015\n",
    "center_dropsondes.data # Print the dropsonde data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ceb6e76-7c0f-428a-9a05-83946bc27056",
   "metadata": {},
   "source": [
    "Tropycal even offers a built-in skew-T plot to quickly check out the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d7ad564f-351b-4ca7-b22c-167e5bd22356",
   "metadata": {},
   "outputs": [],
   "source": [
    "center_dropsondes.plot_skewt()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2266b032-a5d8-4c1f-b77b-1b56094c21bc",
   "metadata": {},
   "source": [
    "What we've gone through above outlines two methods of querying and getting aircraft recon data, which is summarized below:\n",
    "1. Fetch data for a particular basin and season\n",
    "2. Fetch data for a particular storm in that basin/season\n",
    "3. Load the recon data for that storm\n",
    "\n",
    "Then,\n",
    "\n",
    "**Method 1**\n",
    "- Get a list of missions using `get_mission()`\n",
    "- Locate a specific mission, get the mission data, and then load the individual recon datasets\n",
    "\n",
    "**Method 2**\n",
    "- Get all recon data from the storm\n",
    "- Query data using `.sel()` "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed548e23-bd81-4599-943e-787307030739",
   "metadata": {},
   "source": [
    "### Realtime data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e656c314-3935-447e-8f42-880b2267d93f",
   "metadata": {},
   "source": [
    "There aren't any active tropical cyclones with reconnaissance flights today, but Tropycal also supports realtime functionality for recon data. We won't be exploring this today due to the lack of storms, but short the `recon.RealtimeRecon()` function creates object that contains the latest recon data based on user preference. Functions associated with this object can be found on the relevant [documentation page](https://tropycal.github.io/tropycal/api/generated/tropycal.recon.RealtimeRecon.html#tropycal.recon.RealtimeRecon)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f3382142-fd65-480a-8874-6e5cc8a366f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# curr_recon = recon.RealtimeRecon( hours = 6 ) # Get the last 6 hours of realtime reconnaissance data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "119774a5-0f72-422f-8227-eb7fafd573d8",
   "metadata": {},
   "source": [
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d667576d-d438-43a3-82e5-34ba7b6d4ac2",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 4. Other helpful tools"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a9fb869-aec7-49ae-8490-206dcf96a6e8",
   "metadata": {},
   "source": [
    "Tropycal has a lot of additional features, and we haven't covered all of the nuances today - you can always refer to the [Tropycal API Reference](https://tropycal.github.io/tropycal/api/index.html) for more information on individual functions. To motivate some of your uses for Tropycal, here are a few interesting use cases:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d30ad188-f7ed-40ab-8ac6-54cd9daea026",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Tropical Cyclone Rainfall"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "862107d4-2b9e-405b-80a6-d6181d41d851",
   "metadata": {},
   "source": [
    "For a limited selection of storms affecting the United States, the [Weather Prediction Center](https://www.wpc.ncep.noaa.gov/#page=ovw) has assembled a database of rainfall totals from weather stations affected by certain tropical cyclones. These have been integrated into Tropycal under the `tropycal.rain` package, which we imported earlier as `rain`. This quick snippet loads data from [Hurricane Carla](https://en.wikipedia.org/wiki/Hurricane_Carla), which caused heavy rainfall in southern Wisconsin despite initially hitting the Texas coast."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56d22ca9-67f4-4250-8476-7ffbdd35066a",
   "metadata": {},
   "outputs": [],
   "source": [
    "rain_obj = rain.RainDataset() # Get the rainfall dataset\n",
    "\n",
    "# Select an individual storm's rainfall data\n",
    "storm = atl_basin.get_storm(('carla',1961))\n",
    "carla_rain = rain_obj.get_storm_rainfall( storm )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d1f68ac3-fe91-4dbd-9db1-75e190a298f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "carla_rain # Pandas dataframe of station observations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "163c96d1-acbd-450c-9764-9ceb4637f8be",
   "metadata": {},
   "source": [
    "In addition to extracting individual station observations, we can also interpolate the rainfall data onto an xarray grid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1bebfd95-f426-49bc-a00c-dcb4a1ec6565",
   "metadata": {},
   "outputs": [],
   "source": [
    "carla_rain_grid = rain_obj.interpolate_to_grid(storm, grid_res=0.1, method='linear', return_xarray=True) # Interpolate Carla's rainfall data to a grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f3a459f2-3cff-4896-b76b-3ec7c89f11f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the interpolation\n",
    "fig, ax = plt.subplots(1,1, subplot_kw = {'projection': ccrs.PlateCarree(), 'extent': [-105,-80,25,45]})\n",
    "\n",
    "carla_rain_grid.plot(ax = ax, cmap='YlGnBu', cbar_kwargs={'label': 'Rainfall [in.]', 'orientation': 'horizontal', 'fraction':0.046})\n",
    "ax.coastlines()\n",
    "ax.gridlines(draw_labels=True)\n",
    "\n",
    "plt.title('Rainfall from Hurricane Carla (1961)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eab34ffa-307c-4942-9ef4-75daddbf14c8",
   "metadata": {},
   "source": [
    "### Tropical Cyclone Analogs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d415b447-584c-48e4-9b21-f81f89d462e8",
   "metadata": {},
   "source": [
    "As opposed to searching for a specific storm by season, we can also search for storms by a user-specified area. This may be either in terms of storms approaching a certain distance from a point, as demonstrated below, or passing through a polygonal area (see the documentation for [`analogs_from_shape()`](https://tropycal.github.io/tropycal/api/generated/tropycal.tracks.TrackDataset.analogs_from_shape.html#tropycal.tracks.TrackDataset.analogs_from_shape)). In this first code segment, we will look for storms passing within 400 km of Madison. This will output a dictionary of storm IDs for the matching storms and the distance from the point in kilometers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2f9eb70-01e2-4f01-b39b-6f472d121fc7",
   "metadata": {},
   "outputs": [],
   "source": [
    "analogs = atl_basin.analogs_from_point((43.07, -89.40),radius=400) # Radius is in km by default\n",
    "analogs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9605c20-3b94-46fd-b20b-8c443b1ee0a1",
   "metadata": {},
   "source": [
    "As you have probably guessed, there is a built-in plotting function accompanying this functionality as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6a178e9e-7752-43b2-b77d-17b4afdde208",
   "metadata": {},
   "outputs": [],
   "source": [
    "atl_basin.plot_analogs_from_point((43.07, -89.40),radius=400)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "AOS573-Tutorials",
   "language": "python",
   "name": "aos573-tutorials"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "toc-autonumbering": false
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
